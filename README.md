# Hugo Bulma MD Test Drive

An example of Hugo site using Bulma Material Design for personal learning purpose.

> Hugo (Chroma) + Bulma (Material Design)

![Hugo Bulma MD: Tutor][hugo-bulma-md-preview]

-- -- --

## Links

### Hugo Step By Step

This repository:

* [Hugo Step by Step Repository][tutorial-hugo-md]

[tutorial-hugo-md]:     https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/

### HTML Step By Step

Additional guidance:

* [Bulma Material Design Step by Step Repository][tutorial-bulma-md] (frankenbulma)

* [Bulma Step by Step Repository][tutorial-bulma]

* [Materialize Step by Step Repository][tutorial-materialize]

* [Semantic UI Step by Step Repository][tutorial-semantic-ui] (frankensemantic)

[tutorial-bulma-md]:    https://gitlab.com/epsi-rns/tutor-html-bulma-md/
[tutorial-bulma]:       https://gitlab.com/epsi-rns/tutor-html-bulma/
[tutorial-materialize]: https://gitlab.com/epsi-rns/tutor-html-materialize/
[tutorial-semantic-ui]: https://gitlab.com/epsi-rns/tutor-html-semantic-ui/

### Comparison

Comparation with other static site generator

* [Eleventy (Materialize) Step by Step Repository][tutorial-11ty-m]

* [Eleventy (Bulma MD) Step by Step Repository][tutorial-11ty-b]

* [Jekyll Step by Step Repository][tutorial-jekyll]

* [Hugo Step by Step Repository][tutorial-hugo]

* [Hexo Step by Step Repository][tutorial-hexo]

* [Pelican Step by Step Repository][tutorial-pelican]

[tutorial-pelican]:     https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/
[tutorial-jekyll]:      https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/
[tutorial-hugo]:        https://gitlab.com/epsi-rns/tutor-hugo-bulma/
[tutorial-11ty-m]:      https://gitlab.com/epsi-rns/tutor-11ty-materialize/
[tutorial-11ty-b]:      https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/
[tutorial-hexo]:        https://gitlab.com/epsi-rns/tutor-hexo-bulma/

### Presentation

* [Concept SSG - Presentation Slide][ssg-presentation]

* [Concept CSS - Presentation Slide][css-presentation]

[ssg-presentation]:     https://epsi-rns.gitlab.io/ssg/2019/02/17/concept-ssg/
[css-presentation]:     https://epsi-rns.gitlab.io/frontend/2019/02/15/concept-css

-- -- --

## Chapter Step by Step

### Tutor 01

> Generate Only Pure HTML

* Setup Directory for Minimal Hugo

* General Layout: Base, List, Single

* Partials: HTML Head, Header, Footer

* Additional Layout: Post

* Basic Content

![Hugo Bulma MD: Tutor 01][hugo-bulma-md-preview-01]

-- -- --

### Tutor 02

* Add Pure Bulma CSS

* Standard Header (jquery or vue) and Footer

* Enhance All Layouts with Bulma CSS

![Hugo Bulma MD: Tutor 02][hugo-bulma-md-preview-02]

-- -- --

### Tutor 03

* Add Custom SASS (Custom Design)

* Nice Header and Footer

* General Layout: Terms, Taxonomy

* Apply Two Column Responsive Layout for Most Layout

* Nice Tag Badge in Tags Page

![Hugo Bulma MD: Tutor 03][hugo-bulma-md-preview-03]

-- -- --

### Tutor 04

* More Content: Lyrics and Quotes. Need this content for demo

* Additional Layout: Archives

* Custom Output: YAML, TXT, JSON

* Article Index: By Year, List Tree (By Year and Month)

![Hugo Bulma MD: Tutor 04][hugo-bulma-md-preview-04]

-- -- --

### Tutor 05

* Post: Header, Footer, Navigation

> Optional Feature

* Blog Pagination: Adjacent, Indicator, Responsive

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

* Article Index: Apply Box Design

![Hugo Bulma MD: Tutor 05][hugo-bulma-md-preview-05]

-- -- --

### Tutor 06

> Finishing

* Article Index: Apply Multi Column Responsive List

* Layout: Service

* Post: Markdown Content

* Post: Table of Content

* Post: Responsive Images

* Post: Shortcodes

* Post: Syntax Highlight

* Post: Bulma Title: CSS Fix

* Meta: HTML, SEO, Opengraph, Twitter

* RSS: Filter to Show Only Post Kind

* Search: lunr.js

![Hugo Bulma MD: Tutor 06][hugo-bulma-md-preview-06]

-- -- --

What do you think ?

[hugo-bulma-md-preview]:   https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/hugo-bulma-md-preview.png
[hugo-bulma-md-preview-01]:https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/step-01/hugo-bulma-md-preview.png
[hugo-bulma-md-preview-02]:https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/step-02/hugo-bulma-md-preview.png
[hugo-bulma-md-preview-03]:https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/step-03/hugo-bulma-md-preview.png
[hugo-bulma-md-preview-04]:https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/step-04/hugo-bulma-md-preview.png
[hugo-bulma-md-preview-05]:https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/step-05/hugo-bulma-md-preview.png
[hugo-bulma-md-preview-06]:https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/step-06/hugo-bulma-md-preview.png
