### Tutor 01

> Generate Only Pure HTML

* Setup Directory for Minimal Hugo

* General Layout: Base, List, Single

* Partials: HTML Head, Header, Footer

* Additional Layout: Post

* Basic Content

![Hugo Bulma MD: Tutor 01][hugo-bulma-md-preview-01]

[hugo-bulma-md-preview-01]:https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/step-01/hugo-bulma-md-preview.png
