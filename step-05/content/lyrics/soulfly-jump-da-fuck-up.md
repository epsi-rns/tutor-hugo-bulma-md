+++
type       = "post"
title      = "Soulfly - Jump Da Fuck Up"
date       = 2018-09-07T07:35:05+07:00
categories = ["lyric"]
tags       = ["nu metal", "2000s"]
slug       = "soulfly-jump-da-fuck-up"
author     = "epsi"
+++

You seem to sever all my frequencies
I'm tethered to your energies
<!--more-->
And everything turns inside out
I can't be killed but I'm not too proud

All this is making things a bit insane
And I don't care who stares or stays
The only thing that matters is
Will you reach out if you can't resist?
