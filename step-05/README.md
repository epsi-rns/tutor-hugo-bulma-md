### Tutor 05

* Post: Header, Footer, Navigation

> Optional Feature

* Blog Pagination: Adjacent, Indicator, Responsive

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

* Article Index: Apply Box Design

![Hugo Bulma MD: Tutor 05][hugo-bulma-md-preview-05]

[hugo-bulma-md-preview-05]:https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/step-05/hugo-bulma-md-preview.png
