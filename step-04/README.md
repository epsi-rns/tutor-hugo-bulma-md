### Tutor 04

* More Content: Lyrics and Quotes. Need this content for demo

* Additional Layout: Archives

* Custom Output: YAML, TXT, JSON

* Article Index: By Year, List Tree (By Year and Month)

![Hugo Bulma MD: Tutor 04][hugo-bulma-md-preview-04]

[hugo-bulma-md-preview-04]:https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/step-04/hugo-bulma-md-preview.png
