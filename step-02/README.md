### Tutor 02

* Add Pure Bulma CSS

* Standard Header (jquery or vue) and Footer

* Enhance All Layouts with Bulma CSS

![Hugo Bulma MD: Tutor 02][hugo-bulma-md-preview-02]

[hugo-bulma-md-preview-02]:https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/step-02/hugo-bulma-md-preview.png
