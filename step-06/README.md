### Tutor 06

> Finishing

* Article Index: Apply Multi Column Responsive List

* Layout: Service

* Post: Markdown Content

* Post: Table of Content

* Post: Responsive Images

* Post: Shortcodes

* Post: Syntax Highlight

* Post: Bulma Title: CSS Fix

* Meta: HTML, SEO, Opengraph, Twitter

* RSS: Filter to Show Only Post Kind

* Search: lunr.js

![Hugo Bulma MD: Tutor 06][hugo-bulma-md-preview-06]

[hugo-bulma-md-preview-06]:https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/step-06/hugo-bulma-md-preview.png
