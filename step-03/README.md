### Tutor 03

* Add Custom SASS (Custom Design)

* Nice Header and Footer

* General Layout: Terms, Taxonomy

* Apply Two Column Responsive Layout for Most Layout

* Nice Tag Badge in Tags Page

![Hugo Bulma MD: Tutor 03][hugo-bulma-md-preview-03]

[hugo-bulma-md-preview-03]:https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/step-03/hugo-bulma-md-preview.png
